var mysql = require('mysql');
var dotenv = require('dotenv');
dotenv.config({ path: ' .././.env' });
var path = require('path');
const url = require('url');   


// public foldeer
var app = require('.././app.js')
var express = require('express');
// JWT for tokens
// ORM Prisma
const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient()

// to hash the password
const byrpt = require('bcryptjs');

//Change seetings
exports.changeSettings = async(req,res) => {
    id=req.session.userInfos[0].id;
    const username=req.body.username;
    const email = req.body.email;
    const country = req.body.country;
    const city=req.body.city;

    if(req.body.password || req.body.confirmPassword) {
        if(req.body.password == req.body.confirmPassword){
            const password = req.body.password;
            //Update avec password
            let passwordHashed = await byrpt.hash(password, 10);
            const updateUser = await prisma.users.update({
                where: {
                    id:id,
                },
                data: {
                  email: email,
                  username :username,
                  country :country,
                  city: city,
                  password :passwordHashed
                },
              })
              return res.redirect(url.format({
                pathname:"/settings",
                query:{
                    status:200,
                    message:'Update success'
                }
            }))
        }else{
            return res.redirect(url.format({
                pathname:"/settings",
                query:{
                    status:500,
                    message:'password is invalid'
                }
            }))
        }
    }else{
        //Update normal
        const updateUser = await prisma.users.update({
            where: {
              id:id,
            },
            data: {
              email: email,
              username :username,
              country :country,
              city: city
            },
          })
          return res.redirect(url.format({
            pathname:"/settings",
            query:{
                status:200,
                message:'Update success'
            }
        }))
    }

}
//Change settings


exports.index = async (req,res)=>{
    const categories = await prisma.categories.findMany();
    console.log();
    
    if(req.session.userInfos){
        res.render('index', { title: 'Home', isLogged: 1, username: req.session.userInfos[0].username, categories:JSON.stringify(categories)});
    }else{
        res.render('index', { title: 'Home', isLogged: 0, username:"", categories:JSON.stringify(categories)});
    }
}   

exports.upload = async (req,res)=>{
    if(req.session.userInfos){
        res.render('upload', { title: 'Upload Video' , isLogged: 1, username: req.session.userInfos[0].username});
    }else{
        res.render('upload', { title: 'Upload Video' , isLogged: 0, username: ""});
    }
}

exports.channels = async (req,res)=>{
    if(req.session.userInfos){
        res.render('channels', { title: 'Channels', isLogged:1, username: req.session.userInfos[0].username});
    }else{
        res.render('channels', { title: 'Channels', isLogged:0,username:""});        
    }
}

exports.single_channel = async (req,res)=>{
    if(req.session.userInfos){
        res.render('single_channel', { title: 'Single_Channel', isLogged: 1, username: req.session.userInfos[0].username});
    }else{
        res.render('single_channel', { title: 'Single_Channel', isLogged:0,username: ""});  
    }
}

exports.live = async (req,res)=>{
    const categories = await prisma.categories.findMany();

    if(req.session.userInfos){
        res.render('live', { title: 'Live', isLogged: 1, username: req.session.userInfos[0].username, categories:JSON.stringify(categories)});
    }else{
        res.render('live', { title: 'Live', isLogged: 0,username: "", categories:JSON.stringify(categories)});
    }    
}

exports.history = async (req,res)=>{
    if(req.session.userInfos){
        res.render('history', { title: 'History', isLogged:1, username: req.session.userInfos[0].username});
    }else{
        res.render('history', { title: 'History', isLogged:0,username: ""});
    }  
}

exports.account = async (req,res)=>{
    const categories = await prisma.categories.findMany();

    if(req.session.userInfos){
        res.render('account', { title: 'Account', isLogged:1, username: req.session.userInfos[0].username, categories:JSON.stringify(categories)});
    }else{
        res.render('account', { title: 'Account', isLogged:0,username: "", categories:JSON.stringify(categories)});    
    }
}

exports.settings = async (req,res)=>{
    const categories = await prisma.categories.findMany();
    if(req.session.userInfos){
        res.render('settings', { title: 'Settings',isLogged:1, username: req.session.userInfos[0].username, data:req.session.userInfos[0], categories:JSON.stringify(categories)});
    }else{
        res.render('settings', { title: 'Settings',isLogged:0 ,username: "", categories:JSON.stringify(categories)});   
    }
}

exports.access_denied = async (req,res)=>{
    const categories = await prisma.categories.findMany();
    if(req.session.userInfos){
        res.render('access_denied', { title: 'Access denied',isLogged:1, username: req.session.userInfos[0].username, data:req.session.userInfos[0], categories:JSON.stringify(categories)});
    }else{
        res.render('access_denied', { title: 'Access denied',isLogged:0 ,username: "", categories:JSON.stringify(categories)});   
    }
}


