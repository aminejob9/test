var express = require('express');
var router = express.Router();
var mysql = require('mysql');
var dotenv = require('dotenv');
dotenv.config({ path:' .././.env'});
var main = require('../Controllers/main');
var oauth = require('../Controllers/Auth');
const check_connexion = require('../middlewares/check_connexion');


/* Auth Routes */
router.get('/login',check_connexion.checkIfUserIsNotConnected,oauth.login);
router.post('/oauth',oauth.oauth);
router.get('/auth/api/discord/redirect',oauth.discord)

router.get('/auth/api/google/redirect',oauth.google)
router.get('/register',oauth.register);

router.get('/register',check_connexion.checkIfUserIsNotConnected,oauth.register);

router.post('/signup',oauth.signup);
router.get('/signup/verify',oauth.verify);
router.get('/logout',oauth.logout);
router.post('/changeSettings',main.changeSettings);

/* GET home page. */
router.get('/',main.index);
router.get('/upload', check_connexion.checkIfUserIsConnected, main.upload);
router.get('/channels',main.channels);
router.get('/single_channel',main.single_channel);
router.get('/live', check_connexion.checkIfUserIsConnected ,main.live);
router.get('/history', check_connexion.checkIfUserIsConnected ,main.history);
router.get('/account', check_connexion.checkIfUserIsConnected ,main.account);
router.get('/settings', check_connexion.checkIfUserIsConnected ,main.settings);
router.get('/access_denied', check_connexion.checkIfUserIsNotConnected ,main.access_denied);


module.exports = router;
