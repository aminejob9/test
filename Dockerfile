FROM node:14
WORKDIR /app
COPY package.json .

EXPOSE 3000

RUN npm install
RUN npm i -g prisma
RUN npm i -g @prisma/client
RUN npm i -g nodemon
RUN npm install -g

COPY . .
CMD npm start